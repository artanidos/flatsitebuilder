# How To

## Create Debian Package
cd /home/[user]/SourceCode
dpkg -b ./flatsitebuilder flatsitebuilder.deb

## Deinstall Debian Package
sudo apt-get remove flatsitebuilder

## Install Debian Package
sudo dpkg -i flatsitebuilder.deb