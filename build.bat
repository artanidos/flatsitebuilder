rem this only works on Windows
rem adjust the path for binarycreator
rem change version number for executable in this file
rem change the version in config/config.xml
rem change the ReleaseDate in packages/.../meta/package.xml 


pyrcc5 main.qrc -o main_rc.py
pyrcc5 resources.qrc -o resources.py
pyrcc5 plugins/carousel.qrc -o plugins/carousel_rc.py
pyrcc5 plugins/imageeditor.qrc -o plugins/imageeditor_rc.py
pyrcc5 plugins/revolution.qrc -o plugins/revolution_rc.py
pyrcc5 plugins/texteditor.qrc -o plugins/texteditor_rc.py
pyrcc5 plugins/github.qrc -o plugins/github_rc.py
pyrcc5 plugins/shopify.qrc -o plugins/shopify_rc.py
pyrcc5 plugins/markdowneditor.qrc -o plugins/markdowneditor_rc.py

C:\Qt\5.15.2\msvc2019_64\bin\lrelease translation\FlatSiteBuilder_de.ts

rmdir dist\main /s /q
rmdir packages\at.crowdware.flatsitebuilder\data /s /q

pyinstaller -w main.py
mkdir packages\at.crowdware.flatsitebuilder\data
mkdir packages\at.crowdware.flatsitebuilder\data\plugins
mkdir packages\at.crowdware.flatsitebuilder\data\themes
mkdir packages\at.crowdware.flatsitebuilder\data\sources
mkdir packages\at.crowdware.flatsitebuilder\data\translation
mkdir packages\at.crowdware.flatsitebuilder\data\icon
xcopy dist\main\*.* packages\at.crowdware.flatsitebuilder\data /E /H /Y
xcopy plugins\*.py packages\at.crowdware.flatsitebuilder\data\plugins /E /H /Y
xcopy themes\*.* packages\at.crowdware.flatsitebuilder\data\themes /E /H /Y
copy translation\FlatSiteBuilder_de.qm packages\at.crowdware.flatsitebuilder\data\translation
copy images\icon_128.ico packages\at.crowdware.flatsitebuilder\data\icon
copy images\icon_128.png packages\at.crowdware.flatsitebuilder\data\icon

rem deactivate experimental plugins like scaffold, shopify
rmdir packages\at.crowdware.flatsitebuilder\data\plugins\scaffold /s /q
del packages\at.crowdware.flatsitebuilder\data\plugins\scaffold*.*
del packages\at.crowdware.flatsitebuilder\data\plugins\shopify*.*

move packages\at.crowdware.flatsitebuilder\data\main.exe packages\at.crowdware.flatsitebuilder\data\FlatSiteBuilder.exe
C:\Qt\Tools\QtInstallerFramework\4.2\bin\binarycreator -f -c config/config.xml -p packages FlatSiteBuilder-Windows-2.1.9.Setup