import FlatSiteBuilder 2.0
import MarkdownEditor 1.0

Content {
    title: "Index"
    menu: "default"
    author: "admin"
    layout: "default"
    date: "2022-01-07"

    Section {

        Row {

            Column {
                span: 12

                Markdown {
                    text: "# Heading level 1
## Heading level 2
### Heading level 3
#### Heading level 4
##### Heading level 5
###### Heading level 6

## Paragraphs
Lorem ipsum dolor **bold** *italic* ***bold italic***  
Lorem ipsum  
Lorem ipsum

## Lists
- Listitem 1
- Listitem 2
- Listitem 3

## Ordered Lists
1. First item
2. Second item
3. Third item
4. Fourth item 

## Nested Ordered Lists
1. First item
2. Second item
3. Third item
    1. Indented item
    2. Indented item
4. Fourth item 

## Tables
| Syntax      | Description | Test Text     |
| :---        |    :----:   |          ---: |
| Header      | Title       | Lorem ipsum dolor   |
| Paragraph   | Text        | Another text      |

## Blockquotes  
&gt;This is a quote
&gt;with a second row

## Blockquotes with nested blockqoutes
&gt; To be or not to be, that is the question. 
&gt;
&gt;&gt; This is a nested quote.


## Blockquotes can contain other Markdown formatted elements.  
Not all elements can be used — you’ll need to experiment to see which ones work.  

&gt; #### We had a greate time yesterday!
&gt;
&gt; - Everythings worked out for us.
&gt; - We don&#x27;t need profit anymore.
&gt;
&gt;  *Everything* is going to be **alright**.


## Horizontal Rules  
***

## Images
![Tux, the Linux mascot](assets/images/tux.png)

## Links  
My favorite search engine is [Startpage](https://startpage.com &quot;The best search engine for privacy&quot;).

## URLs and Email Addresses
URL link: &lt;https://www.crowdware.at&gt;  
Email link: &lt;bla@blub.com&gt;  

## Formatting Links
I love supporting the **[CrowdWare](https://www.crowdware.at)**.
This is the *[Sample Page for FSB](https://artanidos.github.io/FlatSiteBuilder/)*.
See the section on [`code`](#code).

More samples you can find here: [MarkdownGuide](https://www.markdownguide.org/basic-syntax/)
"
                }

                Markdown {
                }
            }
        }
    }
}
